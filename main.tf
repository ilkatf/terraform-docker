# resource "null_resource" "dockervol" {
# provisioner "local-exec" {
# command = "mkdir noderedvol/ || true && sudo chown -R 1000:1000 noderedvol"
# }
# }
#

module "image" {
  source   = "./image"
  for_each = local.deployment
  image_in = each.value.image
}
#
# resource "random_string" "random" {
# for_each = local.deployment
# length  = 4
# special = false
# upper   = false
# }
#
module "container" {
  source = "./container"
  #depends_on        = [null_resource.dockervol]
  count_in = each.value.container_count
  for_each = local.deployment
  image_in = module.image[each.key].image_out
  #name_in           = join("-", [each.key, terraform.workspace, random_string.random[each.key].result])
  name_in     = each.key
  int_port_in = each.value.int
  ext_port_in = each.value.ext
  #container_path_in = each.value.container_path
  #host_path_in      = "${path.cwd}/noderedvol"
  volumes_in = each.value.volumes
}

# locals {
# container_count = length(var.ext_port[terraform.workspace])
# }
