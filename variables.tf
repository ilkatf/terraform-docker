/* variable "env" { */
#   type = string
#   description = "Env to deploy"
#   default = "dev"
# }
/*  */
variable "image" {
  type        = map(any)
  description = "Image for container"
  default = {
    nodered = {
      dev  = "nodered/node-red:latest"
      prod = "nodered/node-red:latest-minimal"
    }
    influxdb = {
      dev  = "quay.io/influxdb/influxdb:v2.0.2"
      prod = "quay.io/influxdb/influxdb:v2.0.2"
    }
    grafana = {
      dev  = "grafana/grafana"
      prod = "grafana/grafana"
    }
  }
}

variable "ext_port" {
  type = map(any)

  #validation {
  #  condition     = max(var.ext_port...) <= 65535 && min(var.ext_port...) > 0
  #  error_message = "The external port mus be in the valid port range 0 - 65335."
  #}
  #sensitive = true 
}

variable "int_port" {
  type    = number
  default = 1880

  validation {
    condition     = var.int_port == 1880
    error_message = "Port must be 1880."
  }
}
